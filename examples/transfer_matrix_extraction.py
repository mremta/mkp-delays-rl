"""
A short script that illustrates how the R- and T-matrices can be extracted from MADX.
A marker called NEWBEG was manually added to the SPS-sequence beforehand.
"""

import numpy as np
from cpymad.madx import Madx

# start MADX
with open('tempfile', 'w') as f:
    madx = Madx(stdout=f,stderr=f)
    madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

# load SPS sequence and magnet strengths
madx.call(file='data/SPS_LS2_2020-05-26.seq')
madx.call(file='data/lhc_q20.str')

# move the start of the sequence to the marker right after the kickers
madx.input('''SEQEDIT, SEQUENCE=sps;
                FLATTEN;
                CYCLE, START=NEWBEG;
                FLATTEN;
              ENDEDIT;''')

# generate TWISS
madx.command.beam(sequence='sps')
madx.use(sequence='sps')
twiss = madx.twiss(sequence='sps', rmatrix=True, sectormap=True, sectoracc=True)

# retrieve R and T matrix
rmatrix = madx.sectortable()
tmatrix = madx.sectortable2()

# retrieve the index of bpmbv.51303
idx = madx.sequence['sps'].expanded_elements.index('BPMBV.51303')

# Cannot save 3d array to .txt, so reshape
tmatrix_reshaped = tmatrix[idx].reshape(tmatrix[idx].shape[0], -1)

# save R- and T-matrix
np.savetxt('data/R_matrix.txt', X=rmatrix[idx][:6, :6])
np.savetxt('data/T_matrix.txt', X=tmatrix_reshaped)
