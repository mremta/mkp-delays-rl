"""
Optimisation environment to find the ideal time shift vector dtau for the electrical switches
of the MKPs in the SPS injection in order to minimise the horizontal beam oscillations in the SPS

The environment uses the fake_mkp_delays module to simulate the waveforms and the oscillation amplitude arising from it
"""
import typing as t
import datetime
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.axes import Axes

from gym import spaces
from cernml.coi import OptEnv
from fake_mkp_delays import FakeMKPdelays  # import physics simulation


class MKPOptEnv(OptEnv):

    def __init__(
            self,
            dtc=200,
            max_steps=1000,  # maximal episode length
            max_rise_time_change=0.2,
            action_scaling=0.005,
            use_hidden_delays=False,
            verbose=False
    ) -> None:
        """
        :param dtc: Batch spacing in nanoseconds.
        :param max_steps: Truncation after max steps.
        :param max_rise_time_change: Waveforms are stretched by up to (1 + max_rise_time_change).
        :param action_scaling: Controls the maximum change of delays in each step.
        :param verbose: Controls console output.
        """
        self.verbose = verbose
        self.dtc = dtc  # time shift in ns between injected batch and circulating batch in the SPS
        self.max_steps = max_steps  # maximum episode length
        self.max_rise_time_change = max_rise_time_change
        self.use_hidden_delays = use_hidden_delays

        self.hidden_delays = np.zeros(8)

        self.time_data = np.genfromtxt('data/time_data_centered.txt')

        self.dof = 8
        self.off_set = np.zeros(self.dof)
        self.action_scaling = action_scaling  # scale the action, defining the maximum change in each step

        # Initialize limits
        self.limits = np.array(
            [
                np.array(self.off_set) - 1000,  # individual lower
                np.array(self.off_set) + 1000,  # individual upper
            ]
        )

        # Attributes for reinforcement learning agent
        self.curr_episode = 0
        self.current_step = 0
        self.total_step_counter = 0  # to count number of total steps agent is acting upon

        # create dictionary for the episode data
        self.episode_data = {
            "length": [],
            "actions": [],
            "boundary_violation": [],
            "real_actions": [],
            "all_rewards": [],
            "initial_reward": [],
            "final_reward": [],
            "initial_settings": [],
            "initial_x": [],
            "final_x": [],
            "reward_sum": [],
            "timestamps": [],
            "states": [],
            "mkp_waveform_kick_data": [],
            "rise_time_change": [],
            "hidden_delays": []
        }

        self.log_data = None

        # initiate action space
        high = 1 * np.ones(self.dof)
        low = -1 * np.ones(self.dof)
        self.action_space = spaces.Box(low=low, high=high, dtype=np.float32)
        self.optimization_space = self.action_space

        # observation space
        self.observation_space = spaces.Box(-1., 1., shape=(266,), dtype=np.float64)
        self.figure = None  # enable space for figure if needed

        # initiate simulation script
        self.reset_MKP_delays()

    def reset_MKP_delays(self) -> None:
        """
        Initializes either the simulation of the SPS-injection-system or the module for communication with the real
        machine.

        :return:
        """
        self.mkp_delays = FakeMKPdelays(
            dtc=self.dtc,
            randomize_waveforms=True,
            max_rise_time_change=self.max_rise_time_change
        )

    def seed(self, seed: float) -> None:
        """
        Allows to set a seed for reproducibility.

        :param seed: seed for the random generator.
        :return:
        """
        np.random.seed(seed)

    def step(self, delta_action: np.ndarray) -> tuple[np.ndarray, float, bool, dict]:
        """
        Takes the normalized action of the agent and performs one transition of the MDP.

        :param delta_action: the normalized action proposed by the agent.
        :return: A tuple containing the state, reward, whether the episode is finished and additional information
        (namely truncation vs termination).
        """

        self.current_step += 1
        is_finalized = False
        info = {"TimeLimit.truncated": False}
        state, reward = self._take_action(delta_action)  # take action

        # check if the new absolute action violates the boundaries
        action_ok = self._check_action(self.off_set)

        # Log the data
        self.log_data["actions"].append(self.off_set)
        self.log_data["real_actions"].append(self.real_action)
        self.log_data["timestamp"].append(str(datetime.datetime.now()).replace(" ", "_").replace(":", "-"))
        self.log_data["out"].append(reward)
        self.log_data["state"].append(state)

        if self.current_step >= self.max_steps or not action_ok:
            self.curr_episode += 1
            is_finalized = True
            info["TimeLimit.truncated"] = True  # we don't have terminal states -> episodes end with truncation

            # save episode data
            self.episode_data["length"].append(self.current_step)
            self.episode_data["actions"].append(self.log_data["actions"])
            self.episode_data["boundary_violation"].append(self.log_data["boundary_violation"])
            self.episode_data["real_actions"].append(self.log_data["real_actions"])
            self.episode_data["all_rewards"].append(self.log_data["out"])
            self.episode_data["final_reward"].append(self.log_data["out"][-1])  # final reward obtained
            self.episode_data["reward_sum"].append(np.sum(self.log_data["out"]))  # summed reward per episode
            self.episode_data["rise_time_change"].append(self.mkp_delays.rise_time_change)
            self.episode_data["final_x"].append(((state[-2:] + 1) * 0.015 / 2.0))
            self.episode_data["initial_reward"].append(self.log_data["initial_reward"])
            self.episode_data["initial_settings"].append(self.log_data["initial_settings"])
            self.episode_data["initial_x"].append(self.log_data["initial_x"][0])
            self.episode_data["hidden_delays"].append(self.log_data["hidden_delays"][0])

            if self.verbose:
                print("\n\n------ EPISODE {} with reward sum: {:.3e} -------- \n\n".format(
                    self.curr_episode, np.sum(self.log_data["out"])
                ))

        return state, reward, is_finalized, info

    def compute_single_objective(self, action: np.ndarray) -> float:
        """
        Allows numerical optimizers to interact with the environment.
        Transforms the delays proposed by the optimizer such that they are consistent with the environment conventions.

        :param action: Machine settings (delays) in ns.
        :return: Loss (negative rewards).
        """
        _, reward, _, _ = self.step((self.norm_data(action) - self.off_set) / self.action_scaling)

        return -reward

    def reset(self) -> np.ndarray:
        """
        Draws and augments a new set of waveforms, randomly chooses initial delays and returns initial observation of
        the environment.

        :return: Initial observation of the environment.
        """
        if self.verbose:
            print("\n\nResetting environment...\n\n")

        self.mkp_delays.augment_waveforms()  # reset the MKP delays if episode is done, to reset randomization of waveforms
        self.current_step = 0

        # Keep logged history in memory to pickle, for each episode
        self.log_data = {
            "actions": [],
            "real_actions": [],
            "boundary_violation": 0,
            "bpm_pos": [],
            "out": [],
            "mkp_waveform_kick_data": [],
            "mkp_waveform_time_data": [],
            "state": [],
            "timestamp": [],
            "opt_variables": [],
            "initial_reward": [],
            "initial_settings": [],
            "initial_x": [],
            "hidden_delays": []
        }

        # randomize hidden delays (shifts)
        if self.use_hidden_delays:
            for i in np.arange(0, len(self.hidden_delays)):
                self.hidden_delays[i] = np.random.uniform(low=-15.0, high=15.0)
            general_shift = np.random.uniform(low=-20.0, high=20.0)
            self.hidden_delays += general_shift

        # Also initialize the absolute action
        self.off_set = np.zeros(self.dof)

        # Randomization of the initial delays (x0 action)
        # randomize general delay
        delta_t0 = np.random.normal(4750, 10)

        # randomize individual delays
        dtau0 = np.zeros(self.dof)
        for i in range(0, 8):
            dtau0[i] = np.random.normal(0, 5)

        # limits for action space are +- 50 of the starting delays
        self.x0_action = dtau0 + delta_t0  # work with delays relative to zero

        self.limits = np.array(
            [
                np.array(self.x0_action) - 1000,  # individual lower
                np.array(self.x0_action) + 1000,  # individual upper
            ]
        )

        state, reward = self._take_action(np.zeros(self.dof))

        self.log_data["out"].append(reward)
        self.log_data["initial_reward"].append(reward)
        self.log_data["initial_settings"].append(self.real_action)
        self.log_data["initial_x"].append(((state[-2:] + 1) * 0.015 / 2.0))
        self.log_data["hidden_delays"].append(self.hidden_delays.tolist())

        return state

    def _check_action(self, action: np.ndarray) -> bool:
        """
        :param action: offset (= normalized delays).
        :return: whether the delays are still within the allowed boundaries.
        """

        checked = np.all(action >= -1) and np.all(action <= 1)

        return checked

    def _take_action(self, delta_action: np.ndarray) -> tuple[np.ndarray, float]:
        """
        Translates delta action taken by the agent into new machine settings and
        returns state observation and reward accordingly.

        :param delta_action: The normalized action proposed by the agent.
        :return: Observation of the state after taking the action and reward for the transition.
        """

        # add relative action to absolute action --> working in "deltas"!
        self.off_set = self.off_set + delta_action * self.action_scaling

        # Assign action to state and un-normalise the action
        self.real_action = self.inv_norm_data(self.off_set)

        # Add total number of steps to total step counter
        self.total_step_counter += 1

        # Get reward from model
        reward = self._get_reward(self.real_action)

        # Observe state from MKP waveform rise times
        state = self._observe()

        if self.verbose:
            print("Current step: {}, Reward: {:.3e}".format(self.current_step, reward))

        return state, reward

    def _observe(self) -> np.ndarray:
        """
        Extract and return equidistant grid of the waveforms and horizontal deviations.

        :return: Current observation of the environment.
        """
        raw_state = []
        dtau = self.inv_norm_data(self.off_set)
        # Check at what index to evaluate the MKP kick data
        t = self.mkp_delays.ref_timestamp  # reference time for kick data evaluation
        for i in range(8):  # 8 switches in total to iterate over
            ts = 2 * t - (self.dtc / 2) - dtau[i]
            t_ind = np.argwhere(self.time_data == (ts - (ts % 2)))  # find closest even index corresponding to this time
            min_index = t_ind.item() - 64
            max_index = t_ind.item() + 65  # ensure that the same amount of elements is taken at either side

            # get maximum amplitude for normalization
            max_amplitude = np.max(self.mkp_delays.mkp_waveforms[i * 2])

            kick = self.mkp_delays.mkp_waveforms[(i * 2)][
                   min_index:max_index:4].flatten()  # only append every second waveform (one per switch)
            kick = np.maximum(kick, np.zeros(len(kick)))  # ensure that waveforms are non-negative
            kick = 2 * kick / max_amplitude - 1  # normalize kick
            raw_state.append(np.array(kick))

        raw_state.append((2 * (np.abs(np.array(self.beampos))) / 0.015 - 1))  # add the two deviations
        raw_state = np.array(raw_state, dtype=object).flatten()
        state = np.concatenate(raw_state, axis=0)  # concatenate into 1D array

        return state

    def _get_reward(self, real_action: np.ndarray) -> float:
        """
        Calculates and returns reward w.r.t. the machine settings.

        :param real_action: Machine settings proposed by the agent.
        :return:
        """
        self.beampos = self.mkp_delays.get_positions(real_action + self.hidden_delays)
        self.log_data["bpm_pos"].append(self.beampos)
        x1bar = abs(self.beampos[0])
        x2bar = abs(self.beampos[1])

        sq_sum_bpm = x1bar ** 2 + x2bar ** 2 + (x1bar - x2bar) ** 2

        reward = -1 * sq_sum_bpm
        reward = 2 * (reward - (-1e-3)) / (-1e-4 - (-1e-3)) - 1

        return reward


    def render(self, mode: str = "human") -> t.Any:
        """
        Render the environment.

        :param mode: "human" shows the current waveforms, "matplotlib_figures" just creates the figure object.
        :return:
        """
        if mode == "human":
            _, axes = plt.subplots()
            self.update_axes(axes)
            plt.show()
            return None
        if mode == "matplotlib_figures":
            if self.figure is None:
                self.figure = plt.figure()
                axes = self.figure.subplots()
            else:
                [axes] = self.figure.axes
            self.update_axes(axes)
            return [self.figure]
        return super().render(mode)


    def update_axes(self, axes: Axes) -> None:
        """
        Update plot with current waveforms.

        :param axes: axes object to plot the waveform on.
        :return:
        """
        _ylim = axes.get_ylim()
        axes.clear()

        if self.log_data["bpm_pos"] != []:
            dtau = self.inv_norm_data(self.off_set)
            # Check at what index to evaluate the MKP kick data
            t = self.mkp_delays.ref_timestamp  # reference time for kick data evaluation

            t_inds = []
            for i in range(8):  # 8 switches in total to iterate over
                ts = 2 * t - (self.dtc / 2) - dtau[i]
                t_ind = np.argwhere(self.time_data == (
                        ts - (ts % 2)))  # find closest even index corresponding to this time
                t_inds.append(t_ind)

            switch_count = 0
            for i in range(16):
                if ((i % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                    switch_count += 1
                min_index = t_inds[switch_count].item() - 128
                max_index = t_inds[switch_count].item() + 128
                t_center = np.argwhere(self.time_data == (t - (self.dtc / 2) - ((t - (self.dtc / 2)) % 2))).item()

                axes.plot(
                    self.mkp_delays.time_data[(t_center - 128):(t_center + 128)],
                    1000 * self.mkp_delays.mkp_waveforms[i][min_index:max_index],
                    label="_nolegend_",
                )
        else:
            axes.plot([])
        axes.axvline(x=self.mkp_delays.ref_timestamp, color='b', ls='--', label='Injected beam')
        axes.axvline(x=(self.mkp_delays.ref_timestamp - self.dtc), color='c', ls='--', label='Circulating beam')
        axes.set_ylabel("Kick [mrad]")
        axes.set_xlabel("Time [ns]")
        axes.legend(loc=4)
        axes.grid()

        return None


    def norm_data(self, x_data: np.ndarray) -> np.ndarray:
        """
        | Normalize data between -1 and 1, by performing this operation:
        | x is in the range [a, b]
        | x-a is in the range [0, b-a], and delta = b-a
        | (x-a)/delta is in the range [0, 1]
        | 2*(x-a)/delta is in the range [0, 2]
        | 2*(x-a)/delta -1 is in the range [-1, 1]

        :param x_data: The machine settings in ns.
        :return: The data normalized to [-1, 1].
        """
        delta = self.limits[1, :] - self.limits[0, :]
        x_data_norm = 2 * ((x_data - self.limits[0, :]) / delta) - 1

        return x_data_norm


    def inv_norm_data(self, x_norm: np.ndarray) -> np.ndarray:
        """
        :param x_norm: The normalized data.
        :return: The actual machine settings in ns.
        """
        delta = self.limits[1, :] - self.limits[0, :]
        x_data = (x_norm + 1) / 2 * delta + self.limits[0, :]

        return x_data
    
    
    def get_initial_params(self) -> np.ndarray:
        """
        Returns the initial parameters for the environment. Required for the OptEnv class.
        """
        return np.zeros(self.dof)
