from typing import Any, Dict, Optional

import numpy as np
import optuna
from stable_baselines3.common.callbacks import BaseCallback, EvalCallback
from torch import nn as nn


def sample_ppo_param(trial: optuna.Trial) -> Dict[str, Any]:
    """
    :param trial: Trial to draw hyperparameters for.
    :return: Dictionary containing hyperparameters for PPO.
    """
    batch_size = trial.suggest_categorical("batch_size", [8, 16, 32, 64, 128, 256, 512])
    n_steps = trial.suggest_categorical("n_steps", [8, 16, 32, 64, 128, 256, 512, 1024, 2048])
    gamma = trial.suggest_float("gamma", 0.5, 1)
    learning_rate = trial.suggest_float("learning_rate", 1e-5, 1, log=True)
    lr_schedule = "constant"
    ent_coef = trial.suggest_categorical("ent_coef", [0.0])
    clip_range = trial.suggest_categorical("clip_range", [0.2])
    n_epochs = trial.suggest_categorical("n_epochs", [10])
    gae_lambda = trial.suggest_categorical("gae_lambda", [1.0])
    max_grad_norm = trial.suggest_categorical("max_grad_norm", [0.5])
    vf_coef = trial.suggest_categorical("vf_coef", [0.5])
    width = trial.suggest_categorical("width", [16, 32, 64, 128])
    depth = trial.suggest_categorical("depth", [1, 2])
    ortho_init = False
    activation_fn = trial.suggest_categorical("activation_fn", ["relu"])

    if batch_size > n_steps:
        batch_size = n_steps

    architecture = [width for i in np.arange(0, depth)]
    net_arch = dict(pi=architecture, vf=architecture)

    activation_fn = {"tanh": nn.Tanh, "relu": nn.ReLU, "elu": nn.ELU, "leaky_relu": nn.LeakyReLU}[activation_fn]

    return {
        "n_steps": n_steps,
        "batch_size": batch_size,
        "gamma": gamma,
        "learning_rate": learning_rate,
        "ent_coef": ent_coef,
        "clip_range": clip_range,
        "n_epochs": n_epochs,
        "gae_lambda": gae_lambda,
        "max_grad_norm": max_grad_norm,
        "vf_coef": vf_coef,
        "policy_kwargs": dict(
            net_arch=net_arch,
            activation_fn=activation_fn,
            ortho_init=ortho_init,
        ),
    }


def sample_rppo_param(trial: optuna.Trial) -> Dict[str, Any]:
    """
    :param trial: Trial to draw hyperparameters for.
    :return: Dictionary containing hyperparameters for RecurrentPPO.
    """
    batch_size = trial.suggest_categorical("batch_size", [8, 16, 32, 64, 128, 256, 512])
    n_steps = trial.suggest_categorical("n_steps", [8, 16, 32, 64, 128, 256, 512, 1024, 2048])
    gamma = trial.suggest_float("gamma", 0.5, 1)
    learning_rate = trial.suggest_float("learning_rate", 1e-5, 1, log=True)
    lr_schedule = "constant"
    ent_coef = trial.suggest_float("ent_coef", 0.00000001, 0.1, log=True)
    clip_range = trial.suggest_categorical("clip_range", [0.1, 0.2, 0.3, 0.4])
    n_epochs = trial.suggest_categorical("n_epochs", [1, 5, 10, 20])
    gae_lambda = trial.suggest_categorical("gae_lambda", [0.8, 0.9, 0.92, 0.95, 0.98, 0.99, 1.0])
    max_grad_norm = trial.suggest_categorical("max_grad_norm", [0.3, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 2, 5])
    vf_coef = trial.suggest_float("vf_coef", 0, 1)
    width = trial.suggest_categorical("width", [16, 32, 64, 128])
    depth = trial.suggest_categorical("depth", [1, 2])
    lstm_hidden_size = trial.suggest_categorical("lstm_hidden_size", [8, 16, 32, 64])
    ortho_init = False
    activation_fn = trial.suggest_categorical("activation_fn", ["relu"])

    if batch_size > n_steps:
        batch_size = n_steps

    architecture = [width for i in np.arange(0, depth)]
    net_arch = dict(pi=architecture, vf=architecture)

    activation_fn = {"tanh": nn.Tanh, "relu": nn.ReLU, "elu": nn.ELU, "leaky_relu": nn.LeakyReLU}[activation_fn]

    return {
        "n_steps": n_steps,
        "batch_size": batch_size,
        "gamma": gamma,
        "learning_rate": learning_rate,
        "ent_coef": ent_coef,
        "clip_range": clip_range,
        "n_epochs": n_epochs,
        "gae_lambda": gae_lambda,
        "max_grad_norm": max_grad_norm,
        "vf_coef": vf_coef,
        "policy_kwargs": dict(
            net_arch=net_arch,
            activation_fn=activation_fn,
            ortho_init=ortho_init,
            lstm_hidden_size=lstm_hidden_size
        ),
    }


class TrialEvalCallback(EvalCallback):
    """
    Callback used for evaluating and reporting a trial.
    """

    def __init__(
        self,
        eval_env,
        trial: optuna.Trial,
        callback_after_eval: Optional[BaseCallback] = None,
        n_eval_episodes: int = 5,
        eval_freq: int = 10000,
        deterministic: bool = True,
        verbose: int = 0,
        best_model_save_path: Optional[str] = None,
        log_path: Optional[str] = None,
    ) -> None:
        """
        :param eval_env: gym Environment for evaluation.
        :param trial: trial to evaluate.
        :param callback_after_eval: can be used to perform and additional callback.
        :param n_eval_episodes: number of evaluation episodes run at each callback.
        :param eval_freq: each eval_freq steps the performance is evaluated.
        :param deterministic: whether the agent should be set into deterministic mode.
        :param verbose: controls console output.
        :param best_model_save_path: path to save the best performing agent.
        :param log_path: path to save the logs.
        """

        super().__init__(
            eval_env=eval_env,
            callback_after_eval=callback_after_eval,
            n_eval_episodes=n_eval_episodes,
            eval_freq=eval_freq,
            deterministic=deterministic,
            verbose=verbose,
            best_model_save_path=best_model_save_path,
            log_path=log_path,
        )
        self.trial = trial
        self.eval_idx = 0
        self.is_pruned = False

    def _on_step(self) -> bool:
        """
        Performs callback and checks whether to continue training.

        :return: Whether to continue training.
        """

        if self.eval_freq > 0 and self.n_calls % self.eval_freq == 0:
            continue_training = super()._on_step()
            self.eval_idx += 1
            # report best or report current ?
            # report num_timesteps or elapsed time ?
            self.trial.report(self.last_mean_reward, self.eval_idx)
            # Early stopping if needed (uses callback_after_eval)
            if not continue_training:
                self.is_pruned = True
                return False
            # Prune trial if needed
            if self.trial.should_prune():
                self.is_pruned = True
                return False
        return True
