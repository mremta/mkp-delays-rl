import numpy as np
import pybobyqa

from ..mkpdelays_env import MKPOptEnv


def performance_evaluation_non_recurrent(model,
                                         dtc=200,
                                         action_scaling=0.015,
                                         max_rise_time_change=0.02,
                                         max_steps=1000,
                                         hidden_delays=True,
                                         num_episodes=10,
                                         seed=10815):

    env = MKPOptEnv(dtc=dtc,
                    action_scaling=action_scaling,
                    max_rise_time_change=max_rise_time_change,
                    max_steps=max_steps,
                    use_hidden_delays=hidden_delays,
                    verbose=False)

    env.seed(seed)

    # Evaluate the agent for the specified number of episodes
    for episode in range(num_episodes):
        # Reset the environment for a new episode
        obs = env.reset()

        # Run the episode until termination
        while True:
            # Use the pretrained RL agent to select actions
            action, _ = model.predict(obs, deterministic=True)

            # Take the selected action in the environment
            obs, reward, done, _ = env.step(action)

            # Break the loop if the episode is done
            if done:
                break

    return env.episode_data


def performance_evaluation_recurrent(model,
                                     dtc=200,
                                     action_scaling=0.015,
                                     max_rise_time_change=0.02,
                                     max_steps=1000,
                                     hidden_delays=True,
                                     num_episodes=10,
                                     seed=10815):

    env = MKPOptEnv(dtc=dtc,
                    action_scaling=action_scaling,
                    max_rise_time_change=max_rise_time_change,
                    max_steps=max_steps,
                    use_hidden_delays=hidden_delays,
                    verbose=False)

    env.seed(seed)

    # Evaluate the agent for the specified number of episodes
    for episode in range(num_episodes):
        # Reset the environment for a new episode
        obs = env.reset()

        # reset lstm-states at start of each episode
        episode_starts = np.ones(1, dtype=bool)
        lstm_states = None

        # Run the episode until termination
        while True:
            # Use the pretrained RL agent to select actions
            action, lstm_states = model.predict(obs, state=lstm_states, episode_start=episode_starts, deterministic=True)

            # Take the selected action in the environment
            obs, reward, done, _ = env.step(action)
            episode_starts = np.zeros(1, dtype=bool)

            # Break the loop if the episode is done
            if done:
                break

    return env.episode_data


def minimizing_objective(dtau,
                         env,
                         dtc,
                         x0_action1,
                         x0_action2,
                         x0_action3,
                         x0_action4,
                         x0_action5,
                         x0_action6,
                         x0_action7,
                         x0_action8,
                         x0_action9):

    x0_action = np.array([x0_action1,
                          x0_action2,
                          x0_action3,
                          x0_action4,
                          x0_action5,
                          x0_action6,
                          x0_action7,
                          x0_action8,
                          x0_action9])

    delta = np.ones(9) * dtc
    real_actions = (dtau + 1) / 2 * delta + (x0_action - np.ones(9) * (dtc / 2))

    actions = np.array(real_actions[0:len(real_actions) - 1])
    for i in np.arange(0, len(actions)):
        actions[i] = actions[i] + real_actions[-1]

    reward = env.compute_single_objective(actions)

    return reward


def performance_evaluation_bobyqa(dtc=200,
                                  action_scaling=0.015,
                                  max_rise_time_change=0.02,
                                  max_steps=1000,
                                  hidden_delays=True,
                                  num_episodes=10,
                                  seed=10815):

    env = MKPOptEnv(dtc=dtc,
                    action_scaling=action_scaling,
                    max_rise_time_change=max_rise_time_change,
                    max_steps=max_steps,
                    use_hidden_delays=hidden_delays,
                    verbose=False)

    env.seed(seed)

    result = {
        'final_x': [],
        'steps': [],
        'min_loss': []
    }

    # Evaluate the agent for the specified number of episodes
    for episode in range(num_episodes):
        env.reset()

        x0_action = np.zeros(9)
        for i in np.arange(0, len(env.x0_action)):
            x0_action[i] = env.x0_action[i] - min(env.x0_action)

        x0_action[8] = min(env.x0_action)

        upper_lim = np.ones(9)
        lower_lim = -1 * np.ones(9)

        bounds = (lower_lim, upper_lim)
        dtau0 = np.zeros(9)

        soln = pybobyqa.solve(
            minimizing_objective,
            args=(env, dtc, x0_action),
            x0=dtau0,
            bounds=bounds,
            rhobeg=0.5,
            rhoend=0.02,
            seek_global_minimum=True,
            print_progress=False,
        )

        minimum = minimizing_objective(np.array(soln.x),
                                       env,
                                       dtc,
                                       x0_action[0],
                                       x0_action[1],
                                       x0_action[2],
                                       x0_action[3],
                                       x0_action[4],
                                       x0_action[5],
                                       x0_action[6],
                                       x0_action[7],
                                       x0_action[8])

        result['steps'].append(np.min(np.where(env.log_data['out'] == (minimum * -1))))
        result['min_loss'].append(minimum)
        result['final_x'].append(env.beampos)

    env.close()

    return result
