# mkp-delays-rl



## Getting started

Clone the repository and install dependencies.

- With the provided shell-script:

    Windows:
    ```
    cmd /C install.sh
    ```

    Linux:
    ```
    sh install.sh
    ```

- Manually:
    ```
    pip install setuptools==65.4.1
    pip install wheel==0.38
    pip install . --no-cache-dir
    ```

The folder [examples](https://gitlab.cern.ch/mremta/mkp-delays-rl/-/tree/master/src/examples) contains examples on how to use this repository.

## Environment

The environment allows to control the delays of the SPS injection-kickers, which are called MKPs. It follows the gym interface.

$$
\begin{array}{|l|l|}
\hline
\text{Action Space} & \texttt{Box(-1., 1., (8,), float32)} \\
\hline
\text{Observation Space} & \texttt{Box(-1., 1., (266,), float64)} \\
\hline
\end{array}
$$

### Action Space
The action space is $\left[-1, 1\right]^{8}$, from which the agent selects an action $a_{t}$ at each step $t$. This action is converted into nanoseconds by a factor $\delta_{\text{max}}$ and applied to the current MKP-delays $\tau_{t-1} \in \mathbb{R}_{\geq 0}^{8}$:
$$
    \tau_{t} = \tau_{t-1} + a_{t} \cdot \delta_{\text{max}}.
$$
The conversion factor $\delta_{\text{max}}$ is a configurable parameter of the environment.

### Observation Space

The observation space is $\left[-1, 1\right]^{266}$. There are two parts: the first 264 dimensions contain certain points waveform-data of the MKPs. The last 2 dimensions are the horizontal deviations of the injected and circulating beam respectively.

The simulation of the SPS injection-system provides two data structures as the accelerator does. The waveform data for each magnet (16 waveforms in total, two per switch) and the horizontal deviations of the two beams, $x_{i}$ and $x_{c}$, resulting from these waveforms and the current delays. Any negative values in the waveforms are set to zero. Then the minimum of zero and the maximum of each waveform are used to normalize the waveforms to $\left[-1, 1\right]$. For each of the 8 Switches there is an equidistant grid of 33 data points taken (8 $\cdot$ 33 = 264) from the waveform of its first magnet, covering a time window of 256 ns. The spacing between timestamps is 8 ns and the 17th data point of each switch corresponds to the timestamp exactly halfway between circulating and injected beam. Absolute values of the deviations are taken and normalized with the minimum of zero and the maximum of 0.015. This maximum corresponds to the limits of the BPM-monitor in meters. 

Fig. 1  shows an example of a waveform, the corresponding kicks with a batch spacing of 200 ns and illustrates the effect of taking an action.

<figure>
  <img src="/docs/images/waveform_example.png">
  <figcaption>Figure 1: Example of an action being taken with a singular waveform for visual clarity. The vertical dashed lines indicate the timestamps of the circulating and the injected beam which are spaced by 200 ns. The horizontal dashed lines correspond to the design strength of the kicks for the respective beams. .</figcaption>
</figure>

### Rewards
Let $x_i$ and $x_c$ be horizontal beam deviations of the injected and the circulating beam respectively.

Loss-function:
$$
    L(x_i, x_c) = x_i^2 + x^2_c + (x_i - x_c)^2.
$$

Let $\tilde{R}(x_i, x_c):=-L(x_i, x_c)$ be the base reward-function. The Reward-function is a positive affine transformation of the base reward-function:
$$
    R(x_i, x_c) = 2 \cdot \frac{\tilde{R}(x_i, x_c) + 0.001}{0.001 - 0.0001} - 1
$$


### Starting state
The initial delays $\tau_{0}$ are randomized at the start of each episode according to the following procedure: Draw $x$ from $N(4750, 10)$ and $y_1, \dots, y_8$ from $N(0, 5)$. Then we set
$$
    \tau_{0i} = x + y_i,
$$
with $\tau_{0i}$ denoting the i-th component of $\tau_{0}$.

For each episode one of 80 sets of waveforms is chosen iteratively (each set contains one waveform per magnet, so 16 in total) and augmented. For each switch a stretching factor $f_{i}$ is drawn from Uniform(0, \verb|max_rise_time_change|). Through linear interpolation of adjacent points, new data points are generated until the array containing the respective waveform is lengthened by the stretching factor. As data points are still treated to be separated by \SI{2}{\nano\second}, the waveform is effectively stretched by $1 + f_{i}$ due to this procedure. Note that not the entire waveform is stretched because otherwise we would shift the rising edge by an unrealistic amount, essentially changing the initial delays. Given these initial delays and waveforms, the resulting deviations are calculated.

### Episode end
The task is continuing, so there are no terminal states. Episodes can end for two reasons:
- Truncation: Maximum number of steps (default: 1000) exceeded.
- Truncation: The episode ends when one (or more) of the delays differ by more than 1000 ns from the delays at the start of the episode. 


### Simulation of the SPS-injection-system
With the current delays the kick given to the circulating and the injected beam by the magnets is calculated. Optionally, it can be selected to not evaluate the waveforms exactly at the timestamp of the circulating and injected beam but to rather add a shift for each switch to these timestamps. These shifts $\tau_{\text{hidden}}$ are randomized at the start of each episode:
$$
    (\tau_{\text{hidden}})_i = x + y_{i},
$$
$x \sim U\left[-20, 20\right]$, $y_{i} \sim U\left[-15, 15\right]$. Ideally, the beams would receive exactly the kicks which were specified at the design of the beamline (e.g. for the circulating beam this would be zero). The simulation aggregates the kicks per vacuum tank and assumes that these kicks are applied to the beam at the center of the respective tank (thin kick approximation). The difference between the occurred kicks and the design kicks is calculated $\Delta\theta_{1}, \dots \Delta\theta_{4}$ and the resulting deviations from the reference trajectory are derived. The origin of the reference trajectory is the center of the first vacuum tank. The distance from the origin along the reference trajectory of the centers of the four vacuum tanks are denoted as $k_{1}, \dots k_{4}$ with $k_{1}=0$. The distance of the end-point of the injection-system is given by $s_{1}$ and the distance of the BPM by $s_2$. The initial conditions $X(k_{1})$ are assumed to be zero except for $x^{\prime}$ which is set to the deviation of the first kick. Let $R:=R(s_{1}, s_{2})$ and $T:=T(s_{1}, s_{2})$. The following equations describe the transport from the kickers to the BPM: 

$$
\begin{align}
X(k_{1}) &= (0, \Delta\theta_{1}, 0, 0, 0, 0)^{\top}                                \vphantom{\sum\limits_{i=1}^{6}}\\
X(k_{2}) &= R_{drift}(k_{2} - k_{1}) X(k_{1}) + (0, \Delta\theta_{2}, 0, 0, 0, 0)^{\top}          \vphantom{\sum\limits_{i=1}^{6}}\\
X(k_{3}) &= R_{drift}(k_{3} - k_{2}) X(k_{2}) + (0, \Delta\theta_{3}, 0, 0, 0, 0)^{\top}     \vphantom{\sum\limits_{i=1}^{6}}\\
X(k_{4}) &= R_{drift}(k_{4} - k_{3}) X(k_{3}) + (0, \Delta\theta_{4}, 0, 0, 0, 0)^{\top}     \vphantom{\sum\limits_{i=1}^{6}}\\
X(s_{1}) &= R_{drift}(s_{1} - k_{4}) X(k_{4}) \vphantom{\sum\limits_{i=1}^{6}}              \vphantom{\sum\limits_{i=1}^{6}}\\
X(s_{2}) &= RX(s_{1}) + (\sum\limits_{k=1}^{6}\sum\limits_{j=1}^{6} T_{i,k,j} X_{k}(s_{1}) X_{j}(s_{1}))_{i=1,\dots,6}
\end{align}
$$